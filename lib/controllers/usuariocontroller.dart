
import 'dart:convert';
import 'dart:io';

import 'package:ejemplo/database/database.dart';
import 'package:ejemplo/models/usuarios.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



class UsuarioController{

  late BuildContext context;

  List usuarios = [];

   TextEditingController nombreapellido_con = new TextEditingController();
   TextEditingController correo_con = new TextEditingController();
   TextEditingController password_con = new TextEditingController();
   TextEditingController telefono_con = new TextEditingController();


// Save Usuarios
dynamic guardarUsuarioController()
{
    
    try{               
        
        // Crear un objeto de la clase Modelo Usuario
        User usuario = new User();

        // setearle atributos
        //usuario.id = 0;
        usuario.nombre = nombreapellido_con.text.trim() ;
        usuario.email = correo_con.text.trim();
        usuario.password = password_con.text.trim();
        usuario.telefono = telefono_con.text.trim();

        // crear una instancia de la clase BD PROVIDER
        final respuestadb =  DBProvider.instance.guardarUsuarios(usuario);
        print(respuestadb);
                   
        if(respuestadb != null)
        {
          return respuestadb;
        }
        else
        {
          return null;       
        }
             
    }catch(e)
    
    {
       print('Error:$e');
    }
   
}


// Buscar Usuarios
dynamic buscarUsuarioenBD() async
{
    try{                      
        // Crear un objeto de la clase Modelo Usuario
        User usuario = new User();
        usuario.email = correo_con.text.trim();
        usuario.password = password_con.text.trim();
        // crear una instancia de la clase BD PROVIDER
        final respuestadb =  await DBProvider.instance.getUsuarioporEmail(usuario.email,usuario.password);
        
        //print(respuestadb.length);
                   
        if(respuestadb.length > 2)
        {
          return respuestadb;
        }
        else
        {
          return null;       
        }
             
    }catch(e)
    
    {
       print('Error:$e');
    }
   
}

//listar
dynamic listarUsuarios() async
{
    try{                      
        
        //User usuario = new User();
        //usuario.userFromJson({},DBProvider.instance.getListUsuarios());

        final jsonuser =  await DBProvider.instance.getListUsuarios();

      
                           
             
    }catch(e)
    
    {
       print('Error:$e');
    }
   
}

// actualizar usuario


// Eliminar usuario

}

