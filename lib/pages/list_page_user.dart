import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:ejemplo/pages/register_user_page.dart';
import 'package:ejemplo/controllers/usuariocontroller.dart';



class ListUserPage extends StatefulWidget {
  const ListUserPage({Key? key}) : super(key: key);

  @override
  State<ListUserPage> createState() => _ListUserPageState();
}

class _ListUserPageState extends State<ListUserPage> {
 
    bool isLoadingMore = false;
    //int page = 1;
    //String CodigoTexto="";
    
    UsuarioController _conUC = new UsuarioController();
    
    
    final scrollController = ScrollController();
    
    //StateSetter _setState;
  
  @override
  void initState() {

    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) async{
      
      scrollController.addListener(_scrollListener);
      //await _con.init(context, refresh);
      await _conUC.listarUsuarios();

    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar:  AppBar(
      backgroundColor: Colors.black12,
      title:  Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(height: 10), // Espacio vacío para centrar
          Text('Lista de Usuarios',style:TextStyle(fontSize: 20),),
          Container(height: 10), // Espacio vacío para centrar
          SizedBox(height: 20,),
           Container(

          height: 40, // Ajustar el alto del contenedor
          child:  TextField(
                style: TextStyle(color: Colors.grey), // Color del texto gris (plomo)
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.search,color:Colors.grey ,),
                  hintText: 'Codigo de Usuario',
                  hintStyle: TextStyle(color: Colors.grey), // Color de la pista gris (plomo)
                  //border: OutlineInputBorder(),
                  filled: true,
                  fillColor: Colors.white, // Color de fondo blanco
                // contentPadding: EdgeInsets.all( 10),
                  isDense: true
                ),
              
             onChanged: (text) async{
               // Simulamos una operación asincrónica que toma tiempo
               /* await Future.delayed(Duration(seconds: 1));
                print(text);
                _con.trampas=[];
                CodigoTexto=text.toUpperCase();
                await _con.getTrampas(1,CodigoTexto,'0','0','0').then((value) => refresh());*/
             },
            
             onSubmitted: (String value) async{
              // Esta función se ejecutará cuando el usuario presione "Aceptar" en el teclado
              /* _con.trampas=[];
              CodigoTexto=value.toUpperCase();
              await _con.getTrampas(1,CodigoTexto,"0","0","0").then((value) => refresh());*/
             },
            ),
      ),
        
        ],
      ),
    actions: [
        PopupMenuButton<String>(
          onSelected: (result) async{
            // Lógica al seleccionar una opción del menú
            print("Opción seleccionada: $result");
            //openAlertBox();
          },
          itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
            PopupMenuItem<String>(
              value: 'filter',
              child: Text('Filtros'),
            ),
            /*PopupMenuItem<String>(
              value: 'opcion2',
              child: Text('Opción 2'),
            ),*/
          ],
        ),
      ],
       toolbarHeight: 120, // Ajusta el alto de la barra
    ), // Cierra el APP Bar
        
        body: ListView.builder(
                padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                controller: scrollController,
                itemCount: isLoadingMore
                    ? _conUC.usuarios.length + 1
                    :_conUC.usuarios!=null 
                    ?_conUC.usuarios.length
                    :0,
                itemBuilder: (context, index) {

                  if (index < _conUC.usuarios.length){
                    
                    final trampa = 2;//_con.trampas[index];
                   
                    return GestureDetector(
                      onTap: () {
                        //openAlertBoxDetalle(trampa);
                       // _con.goToDetalle(trampa);
                      },
                      
                      child: Card(
                        color:/*!trampa['estado']? Color(int.parse('#F8EBFA'.replaceAll('#', '0xff'))):*/Colors.white,
                        child: ListTile(
                          leading: Container(
                              width: MediaQuery.of(context).size.width * 0.05,
                              child: 74 != null
                                  ? 74 == 74
                                  ?Image.asset('assets/img/bug.png')
                                  : SizedBox()
                                  :SizedBox()),
                          title: Text(
                            'hola',
                            maxLines: 1,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black),
                          ),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              RichText(
                                text: TextSpan(
                                    style: DefaultTextStyle.of(context).style,
                                    children: <TextSpan>[  
                                      TextSpan(
                                        text: 'muni',
                                      )
                                    ]),
                              ),
                              RichText(
                                text: TextSpan(
                                    style: DefaultTextStyle.of(context).style,
                                    children: [
                                      TextSpan(
                                        text: 'fecha',
                                      )
                                    ]),
                              ),
                            ],
                          ),
                          trailing: Icon(Icons.arrow_forward_ios),
                        ),
                      ),
                    );
                  } 
                  
                  else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
    
                  /* */
                },
              ),

            
            // Widget add User
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => RegisterUserPage(),
                    ),
                  );
                
              },
              child: Icon(Icons.add),
              backgroundColor: Colors.green,
            ),
            
      );
  }

  
  Future<void> _scrollListener() async {
    if (isLoadingMore) return;
    if (scrollController.position.pixels ==
        scrollController.position.maxScrollExtent) {
      setState(() {
        isLoadingMore = false;
      });
    }
  }



    void refresh() {
    if (!mounted) return;
    setState(() {});
  }

}
